ktp-contact-list (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:58:28 +0100

ktp-contact-list (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add/update Albert Astals Cid’s master key in upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 07 Jan 2023 00:08:02 +0100

ktp-contact-list (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.11.90).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.12.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 11 Dec 2022 23:14:52 +0100

ktp-contact-list (21.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Added myself to the uploaders.
  * Bump Standards-Version to 4.6.0, no change required.
  * Drop kde-l10n migration rules, not needed anymore.
  * Drop dbgsym migration rules, not needed anymore.
  * Refresh upstream metadata.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 05 Apr 2022 00:13:28 +0200

ktp-contact-list (21.08.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.08.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 16:31:04 +0900

ktp-contact-list (21.04.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.04.0).
  * Bump libktp-dev dependency to same release.
  * Borrow minimal upstream signing key from k3b.
  * Update watch file for signatures.
  * Drop Kubuntu from maintainer name.
  * Removed Mark Purcell from the uploaders, thanks for your work on the
    package!
  * Added myself to the uploaders.

 -- Norbert Preining <norbert@preining.info>  Thu, 06 May 2021 10:39:59 +0900

ktp-contact-list (20.08.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file to the new release-service location.
  * Switch Vcs-* fields to salsa.debian.org.
  * Update the build dependencies according to the upstream build system:
    - bump libktp-dev to 20.08.0
    - explicitly add gettext
  * Switch from dhmk to the dh sequencer:
    - invoke the dh sequencer using the kf5 addon
    - call the right debhelper command instead of $(overridden_command)
    - manually force the generation of the substvars for the kde-l10n breaks
  * Bump the debhelper compatibility to 13:
    - switch the debhelper build dependency to debhelper-compat 13
    - remove debian/compat
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Update the patches:
    - ftbfs-with-qt-5.11.patch: drop, backported from upstream

 -- Pino Toscano <pino@debian.org>  Thu, 13 Aug 2020 22:08:31 +0200

ktp-contact-list (17.08.3-2) unstable; urgency=medium

  * Team upload.
  * Fix Qt 5.11 FTBFS with upstream patch (Closes: #907231)

 -- John Scott <jscott@posteo.net>  Sat, 07 Dec 2019 19:17:37 -0500

ktp-contact-list (17.08.3-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * New upstream release.
  * Bump Standards-Version to 4.1.2, no changes required.
  * Simplify watch file, and switch it to https.
  * Remove trailing whitespaces in changelog.
  * Stop forcing debian/tmp as local installation directory, installing
    everything to the final location
    - drop kde-telepathy-contact-list.install, no more useful now
  * Bump telepathy-related build dependencies:
    - libktp-dev to >= 17.08.3
    - libtelepathy-logger-qt-dev to >= 17.08
    - libtelepathy-qt5-dev to >= 0.9.7
  * Adjust l10npkgs_firstversion_ok to the version where kde-l10n will
    drop translations.

 -- Pino Toscano <pino@debian.org>  Sat, 09 Dec 2017 08:49:07 +0100

ktp-contact-list (17.08.1-1) experimental; urgency=medium

  * New upstream release (17.08.1).
  * Update watch file
  * Add breaks/replaces against the kde-l10n packages
  * Drop soon to be deprecated fail-missing
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 11 Sep 2017 19:59:06 +0200

ktp-contact-list (16.04.2-1) experimental; urgency=medium

  * New upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Fri, 24 Jun 2016 09:37:30 +0200

ktp-contact-list (16.04.1-1) experimental; urgency=medium

  [ Automatic packaging ]
  * Bump Standards-Version to 3.9.8
  * Update build-deps and deps with the info from cmake
  * upstream branch
  * Add a .gitattributes file to use dpkg-mergechangelogs

 -- Maximiliano Curia <maxy@debian.org>  Sat, 28 May 2016 02:02:29 +0200

ktp-contact-list (15.12.1-2) experimental; urgency=medium

  * Bump build dependencies.

 -- Maximiliano Curia <maxy@debian.org>  Fri, 12 Feb 2016 20:52:57 +0100

ktp-contact-list (15.12.1-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * Update watch file.
  * Bump Standards-Version to 3.9.6.
  * New upstream release (15.12.0).
  * New upstream release (15.12.1).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 01 Feb 2016 10:23:21 +0100

ktp-contact-list (15.08.3-1) unstable; urgency=medium

  [ Diane Trout ]
  * New upstream release.

 -- Diane Trout <diane@ghic.org>  Wed, 02 Dec 2015 21:37:12 -0800

ktp-contact-list (15.08.2-1) unstable; urgency=medium

  * Apply homepage update patch from Matthew Cope
  * Update watch file for KTps move to Applications.
  * New upstream release.
  * Use --fail-missing to make it easier to find not installed files.
  * Update build-depends to use libtelepathy-qt5, libtelepathy-logger-qt-dev,
    qtbase5, extra-cmake-modules and many kf5 libraries.
  * Update copyright file, a few modules are now LGPL, the po/* files
    are somewhere else and I added myself to the debian/ block
  * Update install paths for ktp-contactlist
  * Use dhmk from pkg-kde-tools to build ktp.
    - Change maintainer to debian-qt-kde
    - Update debian/rules
  * Add KDE's unstable download url to watch file

 -- Diane Trout <diane@ghic.org>  Sat, 07 Nov 2015 10:44:51 -0800

ktp-contact-list (0.9.0-1) unstable; urgency=medium

  * New upstream release.
  * Update libktpcommoninternalsprivate-dev build-dep to 0.9.0
  * Update Standards-Version to 3.9.6. No changes needed.
  * Update copyright file.
  * Update debian/watch file to use http://download.kde.org

 -- Diane Trout <diane@ghic.org>  Sat, 25 Apr 2015 19:47:52 -0700

ktp-contact-list (0.8.1-2) unstable; urgency=medium

  * Add kde-telepathy-kpeople as a dependency to ktp-contactlist
    this should force installing of the kpeople components to
    prevent the contact list from being empty.
    (Closes: #753305)

 -- Diane Trout <diane@ghic.org>  Tue, 21 Oct 2014 22:07:01 -0700

ktp-contact-list (0.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.5. No changes needed.
  * Update ktp-common-internals and kde-telepathy-data to 0.8.1
  * Enable kpeople with build-depends >= 0.2.2
  * Update debian/copyright

 -- Diane Trout <diane@ghic.org>  Fri, 16 May 2014 21:01:52 -0700

ktp-contact-list (0.7.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Build-Depends of libktpcommoninternalsprivate-dev to 0.7.0
  * Bump depends kde-telepathy-data to 0.7.0

 -- Diane Trout <diane@ghic.org>  Wed, 27 Nov 2013 18:11:34 -0800

ktp-contact-list (0.6.3-1) unstable; urgency=low

  [ Diane Trout ]
  * Update watch file as KDE-Telepathy is now in KDE stable release.
  * Imported Upstream version 0.6.2
  * Bump ktp-common-internals dependency to 0.6.2
  * Bump libtelepathy-qt4-dev dependency to 0.9.3
  * Add dependency on libtelepathy-logger-qt
  * Set Uploaders to Diane Trout and Michał Zając
  * Update Standards-Version to 3.9.4. No changes needed.

  [ Mark Purcell ]
  * Imported Upstream version 0.6.3
  * Add myself to Uploaders
  * Update Build-Depends: libktpcommoninternalsprivate-dev (>= 0.6.3)

 -- Mark Purcell <msp@debian.org>  Sun, 11 Aug 2013 10:33:39 +1000

ktp-contact-list (0.4.0-1) unstable; urgency=low

  * Initial release.

 -- George Kiagiadakis <kiagiadakis.george@gmail.com>  Mon, 18 Jun 2012 22:20:19 +0300
